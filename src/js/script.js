  var startDate="";
var endDate="";

$(document).ready(
	
  
  /* This is the function that will get executed after the DOM is fully loaded */
  function () {	  
	  
        /* Special date widget */
       
        var to = new Date();
        var from = new Date(2011, 1, 1);
        
        $('#datepicker-calendar').DatePicker({
          inline: true,
          date: [from, to],
		  dateFormat: 'yy-mm-dd' ,
		  changeMonth: true,
          changeYear: true,
		  yearRange: '2011:2014',
		  minDate: new Date(2011, 10 - 1, 25),
          calendars: 3,
          mode: 'range',
          current: new Date(to.getFullYear(), to.getMonth() - 1, 1),
          onChange: function(dates,el) {
            // update the range display
            $('#date-range-field span').text(dates[0].getFullYear()+'-'+(dates[0].getMonth()+1)+'-'+dates[0].getDate()+' to '+
                                        dates[1].getFullYear()+'-'+(dates[1].getMonth()+1)+'-'+dates[1].getDate());
			  var monthstart=(dates[0].getMonth()+1);
			  var monthend=(dates[1].getMonth()+1);
			  var datestart=dates[0].getDate();
			  var dateend=dates[1].getDate();
			  
			  if(monthstart<10)
			  {
			   monthstart='0'+(dates[0].getMonth()+1);
				 
			  }
			  
			  if(monthend<10)
			  {
			   monthend='0'+(dates[1].getMonth()+1);
				  
			  }
			  
			  if(datestart<10)
			  {
			   datestart='0'+dates[0].getDate();
				  
			  }
			  
			  if(dateend<10)
			  {
			   dateend='0'+dates[1].getDate();
				 
			  }
			  
			 
			startDate=dates[0].getFullYear()+'-'+monthstart+'-'+datestart;
			endDate=dates[0].getFullYear()+'-'+monthend+'-'+dateend;
			   
			  
          }
			
			    
        });
      
        // initialize the special date dropdown field
        $('#date-range-field span').text(from.getFullYear()+'-'+(from.getMonth()+1)+'-'+from.getDate()+' to '+
                                        to.getFullYear()+'-'+(to.getMonth()+1)+'-'+to.getDate());
        
		 
        // bind a click handler to the date display field, which when clicked
        // toggles the date picker calendar, flips the up/down indicator arrow,
        // and keeps the borders looking pretty
        $('#date-range-field').bind('click', function(){
          $('#datepicker-calendar').toggle();
          if($('#date-range-field a').text().charCodeAt(0) == 9660) {
            // switch to up-arrow
            $('#date-range-field a').html('&#9650;');
            $('#date-range-field').css({borderBottomLeftRadius:0, borderBottomRightRadius:0});
            $('#date-range-field a').css({borderBottomRightRadius:0});
          } else {
            // switch to down-arrow
            $('#date-range-field a').html('&#9660;');
            $('#date-range-field').css({borderBottomLeftRadius:5, borderBottomRightRadius:5});
            $('#date-range-field a').css({borderBottomRightRadius:5});
          }
			
          return false;
        });
        
        // global click handler to hide the widget calendar when it's open, and
        // some other part of the document is clicked.  Note that this works best
        // defined out here rather than built in to the datepicker core because this
        // particular example is actually an 'inline' datepicker which is displayed
        // by an external event, unlike a non-inline datepicker which is automatically
        // displayed/hidden by clicks within/without the datepicker element and datepicker respectively
        
        // stop the click propagation when clicking on the calendar element
        // so that we don't close it
        $('#datepicker-calendar').click(function(event){
          event.stopPropagation();
        });
     
      /* End special page widget */
	
  }
);