var googleMap = null;
var positionsArray = new Array();
var elephantLocationArray = new Array();
var setter = 0;
var elephantPath = null;
var position = null;
var interval;
var elephantIterator;


//Image details
var dexieImage = "img/dexie.png";
var female2Image = "img/female2.png";
var janakiImage = "img/janaki.png";
var madhumaliImage = "img/madhumali.png";
var maleImage = "img/male.png";
var samareImage = "img/samare.png";

function initialize(){
	
	var pos = new google.maps.LatLng(7.0000, 81.0000);
	
        var icons = {
          elephant1: {
            name: 'Dexie',
            icon: dexieImage
          },
          elephant2: {
            name: 'Female2',
            icon: female2Image
          },
          elephant3: {
            name: 'Janaki',
            icon: janakiImage
          },
		  elephant4: {
            name: 'Madhumali',
            icon: madhumaliImage
          },
          elephant5: {
            name: 'Male',
            icon: maleImage
          },
          elephant6: {
            name: 'Samare',
            icon: samareImage
          }
        };

	
	var mapOptions = {
		center : pos,
		zoom : 7,
		mapTypeId: google.maps.MapTypeId.TERRAIN
	};
	
	
	var legend = document.getElementById('legendWindow');
        for (var key in icons) {
          var type = icons[key];
          var name = type.name;
          var icon = type.icon;
          var div = document.createElement('div');
          div.innerHTML = '<img src="' + icon + '"> ' + name;
          legend.appendChild(div);
     }
	
	
	googleMap = new google.maps.Map(document.getElementById("mapArea"),mapOptions);
	googleMap.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(
  		document.getElementById('legendWindow'));
	
}

google.maps.event.addDomListener(window, 'load', initialize);

function animatePath(){
	if(elephantPath != null){
				elephantPath.setMap(null);
				clearInterval(interval);
				elephantPath = null;
	}
	
	else if(elephantLocationArray.length > 0){
		Routing.singleElephantRouting(elephantLocationArray);
	}
	else if(setter == 1){
		Routing.singleElephantRouting(elephantLocationArray);
	}
	else{
		alert("Locations are not available");
	}
}


function stopAnimation(){
	
}

function renderMe(){
	
			var url = "/GoogleMapsDemo/trackElephant/trackSingle";
	
			//Show an error if the user havent selected an option
			if($("#elephantSelecter").val() == null){
				$('#myModal').modal('show');
				return false;
			}
			else{
				
				//Resetting the map
				if(positionsArray.length != 0){
					
					//positionsArray[elephantIterator][i]
					
				for(var i=0; i < positionsArray.length;i++){
						positionsArray[i].setMap(null);
					}
					
				if(position!=null){
					position.setMap(null);
				}	
					position = null;
					positionsArray = new Array();
					elephantLocationArray = new Array();
					setter = 1;
					animatePath();
				
				}	
			
				setter = 0;
				
				
			$.each($("#elephantSelecter").val() ,function (index,value) {
				
				//this is one of the selected values
            	var val = value;
				
				if($("#dateRangeEnable").is(':checked')){
					var data = {"func_name" : "getSingleElephantDataWithDataRange", "id" : val,
							"fromDate":startDate, "toDate":endDate};
	
					Link.getDetails(url,"GET","json",data,function(data){Render.printSingleElephant(data)});
				}
				else if($('#specDateEnable').is(':checked')){
					var data = {"func_name":"getSingleElephantDataWithSpecDate","id": val,
							"specDate":$("#specDateText").val()};
					Link.getDetails(url,"GET","json",data,function(data){Render.printSingleElephant(data);});
				}
				
				else if($("#collaring").is(":checked")){
					var data = {"func_name":"getSingleElephantDataCollaringLocation","id": val};
					Link.getDetails(url,"GET","json",data,function(data){Render.printSingleElephantSingleLocation(data);});	
				}
				else if($("#lastLocation").is(":checked")){
					var data = {"func_name":"getSingleElephantDataLastLocation","id": val};
					Link.getDetails(url,"GET","json",data,function(data){Render.printSingleElephantSingleLocation(data);});	
				}
				else{
					var data = {"func_name" : "getSingleElephantData", "id" : val};
					Link.getDetails(url,"GET","json",data,function(data){Render.printSingleElephant(data);});
				}
				
          	});
			list = new Array();
			
	
		}
	
}

Link = new function(){
	
	this.getDetails = function(url,type,dataType,data,callBack){
		$.ajax({
			type : type,
			url : url,
			datatype :dataType,
			data : data,
			traditional: true,
			success : callBack
		});
	}
}

Render = new function(){

	this.printSingleElephant = function(data){
			
			var result = JSON.parse(data);
			
			var positionArrayLength = positionsArray.length;
		
					
			for(var i = 0; i < result.elephantDetails.length;i++){
				
				var location = new google.maps.LatLng(result.elephantDetails[i].y
													  ,result.elephantDetails[i].x);
				
				elephantLocationArray[i] = location;
				
				var image = null;
				
				if(result.elephantDetails[i].Name == "Janaki"){
					image = janakiImage;
				}
				else if(result.elephantDetails[i].Name == "Dexie"){
					image = dexieImage;
				}
				else if(result.elephantDetails[i].Name == "Male"){
					image = maleImage;
				}
				else if(result.elephantDetails[i].Name == "Female2"){
					image = female2Image;
				}
				else if(result.elephantDetails[i].Name == "Samare"){
					image = samareImage;
				}
				else if(result.elephantDetails[i].Name == "Madhumali"){
					image = madhumaliImage;
				}
				else{
					image = janakiImage;
				}
				
				positionsArray[positionArrayLength++] = new google.maps.Marker({
					position : location,
					map : googleMap,
					icon : image,
					title: result.elephantDetails[i].Name
				});
				
				
				
			}
			var newCenter = new google.maps.LatLng(result.elephantDetails[0].y
													  ,result.elephantDetails[0].x);
			googleMap.setZoom(12);
			googleMap.panTo(newCenter);
			elephantIterator++;
			$("#afterEffectsMenu").css("display", "block");
	}
	
	this.printSingleElephantSingleLocation = function(data){
		
		var result = JSON.parse(data);			
				
		var location = new google.maps.LatLng(result.elephantDetails.y
													  ,result.elephantDetails.x);
		var image = null;
				
		
		if(result.elephantDetails.Name == "Janaki"){
			image = janakiImage;
		}
		else if(result.elephantDetails.Name == "Dexie"){
			image = dexieImage;
		}
		else if(result.elephantDetails.Name == "Male"){
			image = maleImage;
		}
		else if(result.elephantDetails.Name == "Female2"){
			image = female2Image;
		}
		else if(result.elephantDetails.Name == "Samare"){
			image = samareImage;
		}
		else if(result.elephantDetails.Name == "Madhumali"){
			image = madhumaliImage;
		}
		
				
		position = new google.maps.Marker({
			position : location,
			map : googleMap,
			icon : image,
			title: result.elephantDetails.Name
		});
				
		var newCenter = new google.maps.LatLng(result.elephantDetails.y
													  ,result.elephantDetails.x);
		googleMap.setZoom(12);
		googleMap.panTo(newCenter);
		
		$("#afterEffectsMenu").css("display", "none");
				
	}
}

Routing = new function(){
	
	this.singleElephantRouting = function(pathData){
		
		if(pathData.length > 0){
			
			var lineSymbol = {
    			path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
    			scale: 4,
				strokeColor: 'EDED24'
  			};
			
			
 var elephantSymbol = {
    path: 'm91.63,351c7.4,-29.63 20.11,-72.10999 40.54,-95c6.7,-7.5 14.31,-14.32001 23.83,-17.94c7.31,-2.77 13.33,-2.17 16.83,-4.64999c2.34,-1.67 7.95999,-9.5 18.17,-16.64c17.78,-12.45 36.74001,-16.01001 58,-15.76999c20.60999,0.24002 46.01999,9.56 64,19.31c0,0 23,13.76001 23,13.76001c3.31,1.03001 11.17999,-0.7 15,-0.98c0,0 31,-1.09001 31,-1.09001c0,0 10,0.92 10,0.92c41.79001,3.34001 77.45999,21.73001 103.34,55.07999c28.01999,36.10999 41.45999,87.20999 46.81003,132c0,0 2.94,34 2.94,34c0,0 0.90997,12 0.90997,12c0,0 0,60 0,60c0,0 -1,18 -1,18c0,0 -2.58002,34 -2.58002,34c-0.32996,2.56 -0.20996,8.66998 -1.58997,10.42999c-1.57001,1.97998 -5.53003,1.57001 -7.83002,1.57001c0,0 -59.00015,0 -59.00015,0c0,0 3,-42 3,-42c0,0 0,-24 0,-24c-0.01999,-12.59 -2.62985,-35.41 -11.20999,-44.95999c-4.69995,-5.23001 -10.23999,-6.73001 -16.79001,-8.41c-9.26999,-2.37985 -24.51001,-5.01001 -34,-5.64984c0,0 -27,-1.98001 -27,-1.98001c-27.82999,-0.04001 -53.32999,-0.66 -81,3.91995c-16.19,2.6799 -33.92996,6.3999 -44.47,20.08005c-6.40985,8.31 -9.50992,19.81 -11.27992,29.99985c-2.25999,13.03998 -2.27,22.95001 -2.25,36c0,0 3,37.03998 3,37.03998c0,0 -12,0 -12,0c0,0 -12,0.96002 -12,0.96002c0,0 -28,0 -28,0c-15.50999,0 -17.92999,1.46002 -18,-9c0,0 0,-12 0,-12c0,0 -1,-13 -1,-13c0,0 0,-24 0,-24c0,0 -1,-17 -1,-17c0,0 1,-19.99985 1,-19.99985c0,0 1.17,-33 1.17,-33c0,0 2.83,-23 2.83,-23c0,0 -49,-9.91 -49,-9.91c-18.74001,-1.07999 -23.77,20.31 -25.87,34.91c-3.28999,22.94 -4.09,45.85001 -4.13,68.99985c0,0 -1,17 -1,17c0,0 0,35 0,35c0,0 -39,-5 -39,-5c0,-68.38 9.07,-163.78986 25.63,-229.99985l0.00002,-0.00003l0.00002,-0.00003l0.00002,-0.00003l0,-0.00003l0.00002,-0.00003z',
    rotation: 90,
    scale: 0.05, 
    strokeOpacity: 1,
    color: 'black',
    fillColor: 'black',
    fillOpacity: 1,
    strokeWeight: 1,
	rotation: 0
  };
			
			/*var symbolOne = {
   					path: 'M 2 1 L 1 0 L 1 5 M -2 1 L -1 0 L -1 5',
    				strokeColor: 'black',
    				fillColor: 'black',
    				fillOpacity: 1
  			};*/
			
			elephantPath = new google.maps.Polyline({
    		path: pathData,
    		geodesic: true,
			icons: [{
      			icon: elephantSymbol,
      			offset: '100%'
    		}]
 			});
			
			elephantPath.setMap(googleMap);
			this.animateCircle(elephantPath);
			
		}else{
			
			if(elephantPath != null){
				elephantPath.setMap(null);
				clearInterval(interval);
				elephantPath = null;
			}
		}
		
	}
	
	this.animateCircle = function(line) {
    var count = 0;
		interval = window.setInterval(function() {
      	count = (count + 1) % 200;

      var icons = line.get('icons');
      icons[0].offset = (count / 2) + '%';
      line.set('icons', icons);
  }, 150);
		
	
	}
	

}


