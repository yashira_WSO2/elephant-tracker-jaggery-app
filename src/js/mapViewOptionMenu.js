var list = new Array();
var count=0;
var element;
function init(){
	
	$('#singleElephantRadio').change(
    function(){
        $("#multipleElephantDiv").css("display", "none");
		$("#singleElephantDiv").css("display", "block");
    });
	
	$('#multipleElephantRadio').change(
    function(){
        $("#singleElephantDiv").css("display", "none");
		$("#multipleElephantDiv").css("display", "block");   
    });
	
	$('#dateRangeEnable').change(
    function(){
		if($("#dateRangeEnable").is(':checked')){
			$("#specDate").css("display","none");
    		$("#dateRangeDiv").css("display","block");
		}
    });
	
	$('#specDateEnable').change(
    function(){
		if($("#specDateEnable").is(':checked')){
			$("#dateRangeDiv").css("display","none");
    		$("#specDate").css("display","block");
		}
    });
	
	$('#collaring').change(
    function(){
		if($("#collaring").is(':checked')){
			$("#dateRangeDiv").css("display","none");
    		$("#specDate").css("display","none");
		}
    });
	
	$('#lastLocation').change(
    function(){
		if($("#specDateEnable").is(':checked')){
			$("#dateRangeDiv").css("display","none");
    		$("#specDate").css("display","none");
		}
    });
	
	$('#dateRangeEnableAll').change(
    function(){
		if($("#dateRangeEnableAll").is(':checked')){
			$("#specDateAll").css("display","none");
    		$("#dateRangeDivAll").css("display","block");
		}
    });
	
	
	$('#specDateEnableAll').change(
    function(){
		if($("#specDateEnableAll").is(':checked')){
			$("#dateRangeDivAll").css("display","none");
    		$("#specDateAll").css("display","block");
		}
    });
	
	$('#collaringAll').change(
    function(){
		if($("#collaringAll").is(':checked')){
			$("#dateRangeDivAll").css("display","none");
    		$("#specDateAll").css("display","none");
			
		}
    });
	$('#lastLocationAll').change(
    function(){
		if($("#lastLocationAll").is(':checked')){
			$("#dateRangeDivAll").css("display","none");
    		$("#specDateAll").css("display","none");
			
		}
    });
	

	
	$("#animatebtnstart").click(function(){
		$("#animatebtnstop").css("display","block");
		$("#animatebtnstart").css("display","none");
	});
	$("#animatebtnstop").click(function(){
		$("#animatebtnstart").css("display","block");
		$("#animatebtnstop").css("display","none");
	});
	
}
$(document).ready(init);